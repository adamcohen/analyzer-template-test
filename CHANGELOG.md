# analyzer-template-test changelog

## v2.12.24

- another new awesome change. (!28)

## v2.12.23

- some new awesome change. (!27)

## v2.12.22

- Generate reports using Dependency Scanning Report format v15 when `DS_SCHEMA_MODEL` is set to `15`. (!363)

## v2.12.21
- Test master branch with ci template and version branches (!24)

## v2.12.20
- Some new change (!23)

## v2.12.19
- Update with changelog entry (!22)

## Unreleased
- Some unreleased feature (!21)

## v2.12.18
- Update with changelog entry (!20)
- Update without changelog entry (!19)

## v2.12.17
- Another important change (!17)
- Really important change (!17)
- Final important change (!17)

## v2.12.16
- Another important change (!16)

## v2.12.15
- Another important change (!15)
- Really important change (!15)
- Final important change (!15)

## v2.12.13
- Another important change (!12)
- Really important change (!12)

## v2.12.12
- Another important change (!11)
- Really important change (!11)

## v2.12.11
- Another important change (!10)
- Really important change (!10)

## v2.12.10
- Another important change (!9)

## v2.12.9
- Another important change (!8)

## v2.12.8
- Another important change (!7)

## v2.12.7
- Another important change (!6)

## v2.12.6
- Another important change (!5)

## v2.12.5
- Another important change (!4)

## v2.12.4
- Bug fix (!3)

## v2.12.3
- Another important change (!2)

## v2.12.2
- Some important change (!1)

## v2.12.1
- Fix a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE`
